# 6. Operadores relacionales

Los operadores son símbolos que permiten realizar operaciones matemáticas, concatenar cadenas, hacer comparaciones.

Oracle reconoce de 4 tipos de operadores:

 
1. Relacionales (o de comparación)
2. Aritméticos
3. De concatenación 
4. Lógicos

Por ahora veremos solamente los primeros.

Los operadores relacionales (o de comparación. nos permiten comparar dos expresiones, que pueden ser variables, valores de campos, etc.

Hemos aprendido a especificar condiciones de igualdad para seleccionar registros de una tabla; por ejemplo:

```sql
select *from libros where autor='Borges';
```
Utilizamos el operador relacional de igualdad.

Los operadores relacionales vinculan un campo con un valor para que Oracle compare cada registro (el campo especificado. con el valor dado.

Los operadores relacionales son los siguientes:

```sh
=	igual
<>	distinto
>	mayor
<	menor
>=	mayor o igual
<=	menor o igual
```

Podemos seleccionar los registros cuyo autor sea diferente de "Borges", para ello usamos la condición:

```sql
 select * from libros
  where autor<>'Borges';
```

Podemos comparar valores numéricos. Por ejemplo, queremos mostrar los títulos y precios de los libros cuyo precio sea mayor a 20 pesos:

```sql
select titulo, precio from libros where precio>20;
```

Queremos seleccionar los libros cuyo precio sea menor o igual a 30:

```sql
select *from librosn where precio<=30;
```

Los operadores relacionales comparan valores del mismo tipo. Se emplean para comprobar si un campo cumple con una condición.

No son los únicos, existen otros que veremos mas adelante.

# Ejercicios de laboratorio

Trabajamos con la tabla "libros" de una librería.
Eliminamos la tabla "libros":

```sql
drop table libros;
```

La creamos con la siguiente estructura:

```sql
create table libros(
    titulo varchar2(30),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(5,2)
);
```

Agregamos registros a la tabla:

```sql
insert into libros (titulo,autor,editorial,precio) values ('El aleph','Borges','Emece',24.50);
insert into libros (titulo,autor,editorial,precio) values ('Martin Fierro','Jose Hernandez','Emece',16.00);
insert into libros (titulo,autor,editorial,precio) values ('Aprenda PHP','Mario Molina','Emece',35.40);
insert into libros (titulo,autor,editorial,precio) values ('Cervantes y el quijote','Borges','Paidos',50.90);
```

Seleccionamos los registros cuyo autor sea diferente de 'Borges':

```sql
select *from libros where autor<>'Borges';
```

Seleccionamos los registros cuyo precio supere los 20 pesos, sólo el título y precio:

```sql
select titulo,precio from libros where precio>20;
```

Note que el valor con el cual comparamos el campo "precio", como es numérico, no se coloca entre comillas. Los libros cuyo precio es menor a 20 pesos no aparecen en la selección.

Recuperamos aquellos libros cuyo precio es menor o igual a 30:

```sql
select *from libros where precio<=30;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(30),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(5,2)
);

insert into libros (titulo,autor,editorial,precio) values ('El aleph','Borges','Emece',24.50);
insert into libros (titulo,autor,editorial,precio) values ('Martin Fierro','Jose Hernandez','Emece',16.00);
insert into libros (titulo,autor,editorial,precio) values ('Aprenda PHP','Mario Molina','Emece',35.40);
insert into libros (titulo,autor,editorial,precio) values ('Cervantes y el quijote','Borges','Paidos',50.90);

select *from libros where autor<>'Borges';

select titulo,precio from libros where precio>20;

select *from libros where precio<=30;
```

# Ejercicios propuesto

## Ejercicio 01

Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

1. Elimine "articulos"

2. Cree la tabla, con la siguiente estructura:

```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(6,2),
    cantidad number(3)
);
 ```

3. Vea la estructura de la tabla.

4. Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);
```

5. Seleccione los datos de las impresoras (2 registros)

6. Seleccione los artículos cuyo precio sea mayor o igual a 400 (3 registros)

7. Seleccione el código y nombre de los artículos cuya cantidad sea menor a 30 (2 registros)

8. Selecciones el nombre y descripción de los artículos que NO cuesten $100 (4 registros)

## Ejercicio 02

Un video club que alquila películas en video almacena la información de sus películas en alquiler en una tabla denominada "peliculas".

1. Elimine la tabla.

```sql
drop table peliculas;
```

2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo:

```sql
create table peliculas(
    titulo varchar2(20),
    actor varchar2(20),
    duracion number(3),
    cantidad number(1)
);
 ```

3. Ingrese los siguientes registros:

```sql
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,4);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,1);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',80,2);
```

4. Seleccione las películas cuya duración no supere los 90 minutos (2 registros)

5. Seleccione el título de todas las películas en las que el actor NO sea "Tom Cruise" (2 registros)

6. Muestre todos los campos, excepto "duracion", de todas las películas de las que haya más de 2 copias (2 registros)